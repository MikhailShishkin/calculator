package org.b00mshaka1aka.operations;

import org.b00mshaka1aka.operations.operation.Operation;

public class CloseBracket extends Operation {
    public CloseBracket() {
        super(")", 0);
    }
}
