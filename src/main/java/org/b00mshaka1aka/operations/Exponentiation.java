package org.b00mshaka1aka.operations;

import org.b00mshaka1aka.operations.operation.BinaryOperation;
import org.b00mshaka1aka.operations.operation.Operation;

public class Exponentiation extends Operation implements BinaryOperation {
    public Exponentiation() {
        super("^", 6);
    }

    @Override
    public double execute(double leftValue, double rightValue) {
        return Math.pow(leftValue, rightValue);
    }
}
