package org.b00mshaka1aka.operations.operation;

public interface PrefixOperation {
    double execute(double value);
}
