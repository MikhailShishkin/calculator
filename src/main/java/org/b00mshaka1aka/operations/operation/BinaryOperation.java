package org.b00mshaka1aka.operations.operation;

public interface BinaryOperation {
    double execute(double leftValue, double rightValue);
}
