package org.b00mshaka1aka.operations.operation;

public interface PostfixOperation {
    double execute(double value);
}
