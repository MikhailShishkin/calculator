package org.b00mshaka1aka.operations.operation;

public abstract class Operation {
    private String sign;
    private int priority;

    public Operation(String sign, int priority) {
        this.sign = sign;
        this.priority = priority;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getSign() {
        return sign;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public int getPriority() {
        return priority;
    }
}
