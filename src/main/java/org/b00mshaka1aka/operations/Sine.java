package org.b00mshaka1aka.operations;

import org.b00mshaka1aka.operations.operation.PrefixOperation;
import org.b00mshaka1aka.operations.operation.Operation;

public class Sine extends Operation implements PrefixOperation {
    public Sine() {
        super("sin", 8);
    }

    @Override
    public double execute(double value) {
        return Math.sin(value);
    }
}
