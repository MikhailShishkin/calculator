package org.b00mshaka1aka.operations;

import org.b00mshaka1aka.operations.operation.BinaryOperation;
import org.b00mshaka1aka.operations.operation.Operation;

public class Division extends Operation implements BinaryOperation {
    public Division() {
        super("/", 4);
    }

    @Override
    public double execute(double leftValue, double rightValue) {
        return leftValue / rightValue;
    }
}
