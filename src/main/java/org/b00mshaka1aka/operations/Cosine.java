package org.b00mshaka1aka.operations;

import org.b00mshaka1aka.operations.operation.PrefixOperation;
import org.b00mshaka1aka.operations.operation.Operation;

public class Cosine extends Operation implements PrefixOperation {
    public Cosine() {
        super("sin", 8);
    }

    @Override
    public double execute(double value) {
        return Math.cos(value);
    }
}
