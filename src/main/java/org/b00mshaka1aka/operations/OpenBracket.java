package org.b00mshaka1aka.operations;

import org.b00mshaka1aka.operations.operation.Operation;

public class OpenBracket extends Operation {
    public OpenBracket() {
        super("(", 0);
    }
}
