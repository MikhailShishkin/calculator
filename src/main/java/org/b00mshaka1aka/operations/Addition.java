package org.b00mshaka1aka.operations;

import org.b00mshaka1aka.operations.operation.BinaryOperation;
import org.b00mshaka1aka.operations.operation.Operation;

public class Addition extends Operation implements BinaryOperation {
    public Addition() {
        super("+", 2);
    }

    @Override
    public double execute(double leftValue, double rightValue) {
        return leftValue + rightValue;
    }
}
