package org.b00mshaka1aka;

import java.util.ArrayList;
import java.util.List;

import org.b00mshaka1aka.operations.*;
import org.b00mshaka1aka.operations.operation.Operation;

public class Operations {
    public static List<Operation> makeOperations() {
        List<Operation> operations = new ArrayList<>();

        operations.add(new Addition());
        operations.add(new Subtraction());
        operations.add(new Multiplication());
        operations.add(new Division());
        operations.add(new Exponentiation());
        operations.add(new Sine());
        operations.add(new OpenBracket());
        operations.add(new CloseBracket());
        operations.add(new Cosine());

        return operations;
    }
}
