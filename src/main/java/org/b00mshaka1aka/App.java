package org.b00mshaka1aka;

import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String expression = in.nextLine();
        Calculator calculator = new Calculator();
        System.out.println(calculator.execute(expression));
    }
}
