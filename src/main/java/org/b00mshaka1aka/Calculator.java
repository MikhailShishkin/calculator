package org.b00mshaka1aka;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import org.b00mshaka1aka.operations.operation.BinaryOperation;
import org.b00mshaka1aka.operations.operation.Operation;
import org.b00mshaka1aka.operations.operation.PostfixOperation;
import org.b00mshaka1aka.operations.operation.PrefixOperation;

public class Calculator {
    private final List<Operation> operations = Operations.makeOperations();

    public double execute(String expression) {
        expression = prepare(expression);
        List<String> expressionList = parse(expression);
        expressionList = toReversePolishNotation(expressionList);
        return executePolishNotationExpression(expressionList);
    }

    private String prepare(String expression) {
        return expression.replaceAll(" ", "");
    }

    private List<String> parse(String expression) {
        List<String> expressionList = new ArrayList<>();

        StringBuilder buffer = new StringBuilder();
        for (int i = 0; i < expression.length();) {
            char character = 0;

            while (i < expression.length()) {
                character = expression.charAt(i);
                if (isNumber(character)) {
                    buffer.append(character);
                    i++;
                } else {
                    break;
                }
            }

            expressionList.add(buffer.toString());
            buffer = new StringBuilder();

            while (i < expression.length()) {
                character = expression.charAt(i);

                if (!isNumber(character)) {
                    buffer.append(character);

                    for (Operation operation : operations) {
                        String bufferString = buffer.toString();
                        if (signInStart(bufferString, operation.getSign())) {
                            expressionList.add(bufferString);
                            buffer = new StringBuilder();
                            break;
                        }
                    }

                    i++;
                    continue;
                } else {
                    break;
                }

            }
        }

        return expressionList;
    }

    private boolean signInStart(String expression, String sign) {
        return expression.indexOf(sign) == 0;
    }

    private List<String> toReversePolishNotation(List<String> expressionList) {
        List<String> outputExpressionList = new ArrayList<>();
        Stack<String> operationsStack = new Stack<>();

        for (String symbol : expressionList) {
            if (isNumber(symbol)) {
                outputExpressionList.add(symbol);
            } else if (isSign(symbol)) {
                Operation operation = getOperationBySign(symbol);

                if (operation instanceof PostfixOperation) {
                    outputExpressionList.add(operation.getSign());
                } else if (operation instanceof PrefixOperation) {
                    operationsStack.push(operation.getSign());
                } else if (operation.getSign().equals("(")) {
                    operationsStack.push(operation.getSign());
                } else if (operation.getSign().equals(")")) {
                    while (!operationsStack.peek().equals("(")) {
                        outputExpressionList.add(operationsStack.pop());
                    }

                    operationsStack.pop();
                } else if (operation instanceof BinaryOperation) {
                    if (!operationsStack.isEmpty()) {
                        Operation peekOperation = getOperationBySign(operationsStack.peek());

                        while ((peekOperation instanceof PrefixOperation ||
                                peekOperation.getPriority() >= operation.getPriority()) &&
                                !operationsStack.isEmpty()) {
                            outputExpressionList.add(operationsStack.pop());

                            if (!operationsStack.isEmpty()) {
                                peekOperation = getOperationBySign(operationsStack.peek());
                            }
                        }
                    }

                    operationsStack.add(operation.getSign());
                }
            }
        }

        while (!operationsStack.isEmpty()) {
            outputExpressionList.add(operationsStack.pop());
        }

        return outputExpressionList;
    }

    private boolean isDigit(char character) {
        return (character >= '0') && (character <= '9');
    }

    private boolean isNumber(char character) {
        return isDigit(character) || (character == '.');
    }

    private boolean isNumber(String string) {
        if (string == null) {
            return false;
        }
        try {
            Double.parseDouble(string);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    private boolean isSign(String string) {
        for (Operation operation : operations) {
            if (operation.getSign().equals(string)) {
                return true;
            }
        }

        return false;
    }

    private Operation getOperationBySign(String sign) {
        for (Operation operation : operations) {
            if (operation.getSign().equals(sign)) {
                return operation;
            }
        }

        return null;
    }

    private double executePolishNotationExpression(List<String> expressionList) {
        Stack<Double> numbers = new Stack<>();
        for (String symbol : expressionList) {
            if (isNumber(symbol)) {
                numbers.push(Double.parseDouble(symbol));
            } else if (isSign(symbol)) {
                Operation operation = getOperationBySign(symbol);

                if (operation instanceof PrefixOperation) {
                    double number = numbers.pop();
                    PrefixOperation operationExecutor = (PrefixOperation) operation;
                    numbers.push(operationExecutor.execute(number));
                } else if (operation instanceof BinaryOperation) {
                    double rightValue = numbers.pop();
                    double leftValue = numbers.pop();

                    BinaryOperation operationExecutor = (BinaryOperation) operation;
                    numbers.push(operationExecutor.execute(leftValue, rightValue));
                }
            }
        }

        return numbers.pop();
    }
}
